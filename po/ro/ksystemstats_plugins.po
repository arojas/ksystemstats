# Sergiu Bivol <sergiu@cip.md>, 2020, 2022.
msgid ""
msgstr ""
"Project-Id-Version: ksysguard\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-27 01:38+0000\n"
"PO-Revision-Date: 2022-02-15 13:31+0000\n"
"Last-Translator: Sergiu Bivol <sergiu@cip.md>\n"
"Language-Team: Romanian\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Lokalize 21.12.2\n"

#: cpu/cpu.cpp:28 disks/disks.cpp:74 disks/disks.cpp:75 gpu/GpuDevice.cpp:20
#: power/power.cpp:39
#, kde-format
msgctxt "@title"
msgid "Name"
msgstr "Denumire"

#: cpu/cpu.cpp:38
#, kde-format
msgctxt "@title"
msgid "Total Usage"
msgstr "Total folosit"

#: cpu/cpu.cpp:39
#, kde-format
msgctxt "@title, Short for 'Total Usage'"
msgid "Usage"
msgstr "Utilizare"

#: cpu/cpu.cpp:45
#, kde-format
msgctxt "@title"
msgid "System Usage"
msgstr "Utilizare sistem"

#: cpu/cpu.cpp:46
#, kde-format
msgctxt "@title, Short for 'System Usage'"
msgid "System"
msgstr "Sistem"

#: cpu/cpu.cpp:52
#, kde-format
msgctxt "@title"
msgid "User Usage"
msgstr "Utilizare utilizator"

#: cpu/cpu.cpp:53
#, kde-format
msgctxt "@title, Short for 'User Usage'"
msgid "User"
msgstr "Utilizator"

#: cpu/cpu.cpp:59
#, kde-format
msgctxt "@title"
msgid "Wait Usage"
msgstr "Utilizare așteptare"

#: cpu/cpu.cpp:60
#, kde-format
msgctxt "@title, Short for 'Wait Load'"
msgid "Wait"
msgstr "Așteptare"

#: cpu/cpu.cpp:85
#, kde-format
msgctxt "@title"
msgid "Current Frequency"
msgstr "Frecvență actuală"

#: cpu/cpu.cpp:86
#, kde-format
msgctxt "@title, Short for 'Current Frequency'"
msgid "Frequency"
msgstr "Frecvență"

#: cpu/cpu.cpp:87
#, kde-format
msgctxt "@info"
msgid "Current frequency of the CPU"
msgstr "Frecvența actuală a procesorului"

#: cpu/cpu.cpp:92
#, kde-format
msgctxt "@title"
msgid "Current Temperature"
msgstr "Temperatură actuală"

#: cpu/cpu.cpp:93
#, kde-format
msgctxt "@title, Short for Current Temperatur"
msgid "Temperature"
msgstr "Temperatură"

#: cpu/cpu.cpp:100
#, kde-format
msgctxt "@title"
msgid "All"
msgstr "Toate"

#: cpu/cpu.cpp:119
#, fuzzy, kde-format
#| msgctxt "@title"
#| msgid "Memory Frequency"
msgctxt "@title"
msgid "Maximum CPU Frequency"
msgstr "Frecvența memoriei"

#: cpu/cpu.cpp:120
#, fuzzy, kde-format
#| msgctxt "@title, Short for 'Current Frequency'"
#| msgid "Frequency"
msgctxt "@title, Short for 'Maximum CPU Frequency'"
msgid "Max Frequency"
msgstr "Frecvență"

#: cpu/cpu.cpp:121
#, fuzzy, kde-format
#| msgctxt "@info"
#| msgid "Current frequency of the CPU"
msgctxt "@info"
msgid "Current maximum frequency between all CPUs"
msgstr "Frecvența actuală a procesorului"

#: cpu/cpu.cpp:126
#, fuzzy, kde-format
#| msgctxt "@title"
#| msgid "Memory Frequency"
msgctxt "@title"
msgid "Minimum CPU Frequency"
msgstr "Frecvența memoriei"

#: cpu/cpu.cpp:127
#, fuzzy, kde-format
#| msgctxt "@title, Short for 'Current Frequency'"
#| msgid "Frequency"
msgctxt "@title, Short for 'Minimum CPU Frequency'"
msgid "Min Frequency"
msgstr "Frecvență"

#: cpu/cpu.cpp:128
#, fuzzy, kde-format
#| msgctxt "@info"
#| msgid "Current frequency of the CPU"
msgctxt "@info"
msgid "Current minimum frequency between all CPUs"
msgstr "Frecvența actuală a procesorului"

#: cpu/cpu.cpp:133
#, fuzzy, kde-format
#| msgctxt "@title"
#| msgid "Memory Frequency"
msgctxt "@title"
msgid "Average CPU Frequency"
msgstr "Frecvența memoriei"

#: cpu/cpu.cpp:134
#, fuzzy, kde-format
#| msgctxt "@title"
#| msgid "Memory Frequency"
msgctxt "@title, Short for 'Average CPU Frequency'"
msgid "Average Frequency"
msgstr "Frecvența memoriei"

#: cpu/cpu.cpp:135
#, fuzzy, kde-format
#| msgctxt "@info"
#| msgid "Current frequency of the CPU"
msgctxt "@info"
msgid "Current average frequency between all CPUs"
msgstr "Frecvența actuală a procesorului"

#: cpu/cpu.cpp:140
#, fuzzy, kde-format
#| msgctxt "@title"
#| msgid "GPU %1 Temperature"
msgctxt "@title"
msgid "Maximum CPU Temperature"
msgstr "Temperatură GPU %1"

#: cpu/cpu.cpp:141
#, fuzzy, kde-format
#| msgctxt "@title, Short for Current Temperatur"
#| msgid "Temperature"
msgctxt "@title, Short for 'Maximum CPU Temperature'"
msgid "Max Temperature"
msgstr "Temperatură"

#: cpu/cpu.cpp:147
#, fuzzy, kde-format
#| msgctxt "@title"
#| msgid "GPU %1 Temperature"
msgctxt "@title"
msgid "Minimum CPU Temperature"
msgstr "Temperatură GPU %1"

#: cpu/cpu.cpp:148
#, fuzzy, kde-format
#| msgctxt "@title, Short for Current Temperatur"
#| msgid "Temperature"
msgctxt "@title, Short for 'Minimum CPU Temperature'"
msgid "Min Temperature"
msgstr "Temperatură"

#: cpu/cpu.cpp:154
#, fuzzy, kde-format
#| msgctxt "@title"
#| msgid "GPU %1 Temperature"
msgctxt "@title"
msgid "Average CPU Temperature"
msgstr "Temperatură GPU %1"

#: cpu/cpu.cpp:155
#, fuzzy, kde-format
#| msgctxt "@title"
#| msgid "Current Temperature"
msgctxt "@title, Short for 'Average CPU Temperature'"
msgid "Average Temperature"
msgstr "Temperatură actuală"

#: cpu/cpu.cpp:171
#, kde-format
msgctxt "@title"
msgid "Number of CPUs"
msgstr "Numărul de procesoare"

#: cpu/cpu.cpp:172
#, kde-format
msgctxt "@title, Short fort 'Number of CPUs'"
msgid "CPUs"
msgstr "Procesoare"

#: cpu/cpu.cpp:173
#, kde-format
msgctxt "@info"
msgid "Number of physical CPUs installed in the system"
msgstr "Numărul de procesoare fizice instalate în sistem"

#: cpu/cpu.cpp:175
#, kde-format
msgctxt "@title"
msgid "Number of Cores"
msgstr "Număr de nuclee"

#: cpu/cpu.cpp:176
#, kde-format
msgctxt "@title, Short fort 'Number of Cores'"
msgid "Cores"
msgstr "Nuclee"

#: cpu/cpu.cpp:177
#, kde-format
msgctxt "@info"
msgid "Number of CPU cores across all physical CPUS"
msgstr "Numărul de nuclee din toate procesoarele fizice"

#: cpu/cpuplugin.cpp:19
#, kde-format
msgid "CPUs"
msgstr "Procesoare"

#: cpu/freebsdcpuplugin.cpp:151
#, kde-format
msgctxt "@title"
msgid "CPU %1"
msgstr "Procesor %1"

#: cpu/linuxcpuplugin.cpp:39
#, kde-format
msgctxt "@title"
msgid "Core %1"
msgstr "Nucleu %1"

#: cpu/linuxcpuplugin.cpp:54
#, kde-format
msgctxt "@title"
msgid "CPU %1 Core %2"
msgstr "Procesor %1 Nucleu %2"

#: cpu/loadaverages.cpp:13
#, kde-format
msgctxt "@title"
msgid "Load Averages"
msgstr "Încărcări medii"

#: cpu/loadaverages.cpp:14
#, kde-format
msgctxt "@title"
msgid "Load average (1 minute)"
msgstr "Încărcare medie (1 min)"

#: cpu/loadaverages.cpp:15
#, kde-format
msgctxt "@title"
msgid "Load average (5 minutes)"
msgstr "Încărcare medie (5 min)"

#: cpu/loadaverages.cpp:16
#, kde-format
msgctxt "@title"
msgid "Load average (15 minute)"
msgstr "Încărcare medie (15 min)"

#: cpu/loadaverages.cpp:18
#, kde-format
msgctxt "@title,  Short for 'Load average (1 minute)"
msgid "Load average (1m)"
msgstr "Încărcare medie (1m)"

#: cpu/loadaverages.cpp:19
#, kde-format
msgctxt "@title,  Short for 'Load average (5 minutes)"
msgid "Load average (5m)"
msgstr "Încărcare medie (5m)"

#: cpu/loadaverages.cpp:20
#, kde-format
msgctxt "@title,  Short for 'Load average (15 minutes)"
msgid "Load average (15m)"
msgstr "Încărcare medie (15m)"

#: cpu/loadaverages.cpp:22
#, kde-format
msgctxt "@info"
msgid "Number of jobs in the run queue averaged over 1 minute"
msgstr "Numărul de lucrări în coada de sarcini, în medie pe 1 minut"

#: cpu/loadaverages.cpp:23
#, kde-format
msgctxt "@info"
msgid "Number of jobs in the run queue averaged over 5 minutes"
msgstr "Numărul de lucrări în coada de sarcini, în medie pe 5 minute"

#: cpu/loadaverages.cpp:24
#, kde-format
msgctxt "@info"
msgid "Number of jobs in the run queue averaged over 15 minutes"
msgstr "Numărul de lucrări în coada de sarcini, în medie pe 15 minute"

#: disks/disks.cpp:78 disks/disks.cpp:258
#, kde-format
msgctxt "@title"
msgid "Total Space"
msgstr "Spațiu total"

#: disks/disks.cpp:80 disks/disks.cpp:259
#, kde-format
msgctxt "@title Short for 'Total Space'"
msgid "Total"
msgstr "Total"

#: disks/disks.cpp:85 disks/disks.cpp:271
#, kde-format
msgctxt "@title"
msgid "Used Space"
msgstr "Spațiu folosit"

#: disks/disks.cpp:87 disks/disks.cpp:272
#, kde-format
msgctxt "@title Short for 'Used Space'"
msgid "Used"
msgstr "Folosit"

#: disks/disks.cpp:92 disks/disks.cpp:264
#, kde-format
msgctxt "@title"
msgid "Free Space"
msgstr "Spațiu liber"

#: disks/disks.cpp:94 disks/disks.cpp:265
#, kde-format
msgctxt "@title Short for 'Free Space'"
msgid "Free"
msgstr "Liber"

#: disks/disks.cpp:100 disks/disks.cpp:278
#, kde-format
msgctxt "@title"
msgid "Read Rate"
msgstr "Rată de citire"

#: disks/disks.cpp:102 disks/disks.cpp:279
#, kde-format
msgctxt "@title Short for 'Read Rate'"
msgid "Read"
msgstr "Citire"

#: disks/disks.cpp:106 disks/disks.cpp:284
#, kde-format
msgctxt "@title"
msgid "Write Rate"
msgstr "Rată de scriere"

#: disks/disks.cpp:108 disks/disks.cpp:285
#, kde-format
msgctxt "@title Short for 'Write Rate'"
msgid "Write"
msgstr "Scriere"

#: disks/disks.cpp:113 disks/disks.cpp:294
#, kde-format
msgctxt "@title"
msgid "Percentage Used"
msgstr "Procentaj folosit"

#: disks/disks.cpp:117 disks/disks.cpp:290
#, kde-format
msgctxt "@title"
msgid "Percentage Free"
msgstr "Procentaj liber"

#: disks/disks.cpp:157
#, kde-format
msgid "Disks"
msgstr "Discuri"

#: disks/disks.cpp:256
#, kde-format
msgctxt "@title"
msgid "All Disks"
msgstr "Toate discurile"

#: disks/disks.cpp:291
#, kde-format
msgctxt "@title, Short for `Percentage Free"
msgid "Free"
msgstr "Liber"

#: disks/disks.cpp:295
#, kde-format
msgctxt "@title, Short for `Percentage Used"
msgid "Used"
msgstr "Folosit"

#: gpu/AllGpus.cpp:14
#, kde-format
msgctxt "@title"
msgid "All GPUs"
msgstr "Toate plăcile grafice"

#: gpu/AllGpus.cpp:16
#, kde-format
msgctxt "@title"
msgid "All GPUs Usage"
msgstr "Utilizarea tuturor plăcilor grafice"

#: gpu/AllGpus.cpp:17
#, kde-format
msgctxt "@title Short for 'All GPUs Usage'"
msgid "Usage"
msgstr "Utilizare"

#: gpu/AllGpus.cpp:26
#, kde-format
msgctxt "@title"
msgid "All GPUs Total Memory"
msgstr "Memoria totală a tuturor plăcilor grafice"

#: gpu/AllGpus.cpp:27
#, kde-format
msgctxt "@title Short for 'All GPUs Total Memory'"
msgid "Total"
msgstr "Totală"

#: gpu/AllGpus.cpp:31
#, kde-format
msgctxt "@title"
msgid "All GPUs Used Memory"
msgstr "Memoria folosită a tuturor plăcilor grafice"

#: gpu/AllGpus.cpp:32
#, kde-format
msgctxt "@title Short for 'All GPUs Used Memory'"
msgid "Used"
msgstr "Folosită"

#: gpu/GpuDevice.cpp:24
#, kde-format
msgctxt "@title"
msgid "Usage"
msgstr "Utilizare"

#: gpu/GpuDevice.cpp:30
#, kde-format
msgctxt "@title"
msgid "Total Video Memory"
msgstr "Memorie video totală"

#: gpu/GpuDevice.cpp:32
#, kde-format
msgctxt "@title Short for Total Video Memory"
msgid "Total"
msgstr "Totală"

#: gpu/GpuDevice.cpp:35
#, kde-format
msgctxt "@title"
msgid "Video Memory Used"
msgstr "Memorie video folosită"

#: gpu/GpuDevice.cpp:37
#, kde-format
msgctxt "@title Short for Video Memory Used"
msgid "Used"
msgstr "Folosită"

#: gpu/GpuDevice.cpp:41
#, kde-format
msgctxt "@title"
msgid "Frequency"
msgstr "Frecvență"

#: gpu/GpuDevice.cpp:45
#, kde-format
msgctxt "@title"
msgid "Memory Frequency"
msgstr "Frecvența memoriei"

#: gpu/GpuDevice.cpp:49
#, kde-format
msgctxt "@title"
msgid "Temperature"
msgstr "Temperatură"

#: gpu/GpuDevice.cpp:53 power/power.cpp:106
#, kde-format
msgctxt "@title"
msgid "Power"
msgstr "Putere"

#: gpu/GpuPlugin.cpp:31
#, kde-format
msgctxt "@title"
msgid "GPU"
msgstr "GPU"

#: gpu/LinuxBackend.cpp:54
#, kde-format
msgctxt "@title %1 is GPU number"
msgid "GPU %1"
msgstr "GPU %1"

#: lmsensors/lmsensors.cpp:22
#, kde-format
msgid "Hardware Sensors"
msgstr "Senzori hardware"

#: memory/backend.cpp:17
#, kde-format
msgctxt "@title"
msgid "Physical Memory"
msgstr "Memorie fizică"

#: memory/backend.cpp:18
#, kde-format
msgctxt "@title"
msgid "Swap Memory"
msgstr "Memorie swap"

#: memory/backend.cpp:39
#, kde-format
msgctxt "@title"
msgid "Total Physical Memory"
msgstr "Memorie fizică totală"

#: memory/backend.cpp:40
#, kde-format
msgctxt "@title, Short for 'Total Physical Memory'"
msgid "Total"
msgstr "Totală"

#: memory/backend.cpp:44
#, kde-format
msgctxt "@title"
msgid "Used Physical Memory"
msgstr "Memorie fizică folosită"

#: memory/backend.cpp:45
#, kde-format
msgctxt "@title, Short for 'Used Physical Memory'"
msgid "Used"
msgstr "Folosită"

#: memory/backend.cpp:49
#, kde-format
msgctxt "@title"
msgid "Used Physical Memory Percentage"
msgstr "Procentaj memorie fizică folosită"

#: memory/backend.cpp:53
#, kde-format
msgctxt "@title"
msgid "Free Physical Memory"
msgstr "Memorie fizică liberă"

#: memory/backend.cpp:54
#, kde-format
msgctxt "@title, Short for 'Free Physical Memory'"
msgid "Free"
msgstr "Liberă"

#: memory/backend.cpp:58
#, kde-format
msgctxt "@title"
msgid "Free Physical Memory Percentage"
msgstr "Procentaj memorie fizică liberă"

#: memory/backend.cpp:62
#, kde-format
msgctxt "@title"
msgid "Application Memory"
msgstr "Memorie aplicații"

#: memory/backend.cpp:63
#, kde-format
msgctxt "@title, Short for 'Application Memory'"
msgid "Application"
msgstr "Aplicații"

#: memory/backend.cpp:67
#, kde-format
msgctxt "@title"
msgid "Application Memory Percentage"
msgstr "Procentaj memorie aplicații"

#: memory/backend.cpp:71
#, kde-format
msgctxt "@title"
msgid "Cache Memory"
msgstr "Memorie cache"

#: memory/backend.cpp:72
#, kde-format
msgctxt "@title, Short for 'Cache Memory'"
msgid "Cache"
msgstr "Cache"

#: memory/backend.cpp:76
#, kde-format
msgctxt "@title"
msgid "Cache Memory Percentage"
msgstr "Procentaj memorie cache"

#: memory/backend.cpp:80
#, kde-format
msgctxt "@title"
msgid "Buffer Memory"
msgstr "Memorie-tampon"

#: memory/backend.cpp:81
#, kde-format
msgctxt "@title, Short for 'Buffer Memory'"
msgid "Buffer"
msgstr "Tampon"

#: memory/backend.cpp:82
#, kde-format
msgid "Amount of memory used for caching disk blocks"
msgstr ""
"Cantitatea de memorie folosită pentru prestocarea blocurilor de pe disc"

#: memory/backend.cpp:86
#, kde-format
msgctxt "@title"
msgid "Buffer Memory Percentage"
msgstr "Procentaj memorie-tampon"

#: memory/backend.cpp:90
#, kde-format
msgctxt "@title"
msgid "Total Swap Memory"
msgstr "Memorie swap totală"

#: memory/backend.cpp:91
#, kde-format
msgctxt "@title, Short for 'Total Swap Memory'"
msgid "Total"
msgstr "Totală"

#: memory/backend.cpp:95
#, kde-format
msgctxt "@title"
msgid "Used Swap Memory"
msgstr "Memorie swap folosită"

#: memory/backend.cpp:96
#, kde-format
msgctxt "@title, Short for 'Used Swap Memory'"
msgid "Used"
msgstr "Folosită"

#: memory/backend.cpp:100
#, kde-format
msgctxt "@title"
msgid "Used Swap Memory Percentage"
msgstr "Procentaj memorie cache folosită"

#: memory/backend.cpp:104
#, kde-format
msgctxt "@title"
msgid "Free Swap Memory"
msgstr "Memorie swap liberă"

#: memory/backend.cpp:105
#, kde-format
msgctxt "@title, Short for 'Free Swap Memory'"
msgid "Free"
msgstr "Liberă"

#: memory/backend.cpp:109
#, kde-format
msgctxt "@title"
msgid "Free Swap Memory Percentage"
msgstr "Procentaj memorie swap liberă"

#: memory/memory.cpp:24
#, kde-format
msgctxt "@title"
msgid "Memory"
msgstr "Memorie"

#: network/AllDevicesObject.cpp:16
#, kde-format
msgctxt "@title"
msgid "All Network Devices"
msgstr "Toate dispozitivele de rețea"

#: network/AllDevicesObject.cpp:18 network/AllDevicesObject.cpp:28
#: network/NetworkDevice.cpp:66 network/NetworkDevice.cpp:76
#, kde-format
msgctxt "@title"
msgid "Download Rate"
msgstr "Rată descărcare"

#: network/AllDevicesObject.cpp:19 network/AllDevicesObject.cpp:29
#: network/NetworkDevice.cpp:67 network/NetworkDevice.cpp:77
#, kde-format
msgctxt "@title Short for Download Rate"
msgid "Download"
msgstr "Descărcare"

#: network/AllDevicesObject.cpp:23 network/AllDevicesObject.cpp:33
#: network/NetworkDevice.cpp:71 network/NetworkDevice.cpp:81
#, kde-format
msgctxt "@title"
msgid "Upload Rate"
msgstr "Rată încărcare"

#: network/AllDevicesObject.cpp:24 network/AllDevicesObject.cpp:34
#: network/NetworkDevice.cpp:72 network/NetworkDevice.cpp:82
#, kde-format
msgctxt "@title Short for Upload Rate"
msgid "Upload"
msgstr "Încărcare"

#: network/AllDevicesObject.cpp:38 network/NetworkDevice.cpp:86
#, kde-format
msgctxt "@title"
msgid "Total Downloaded"
msgstr "Total descărcat"

#: network/AllDevicesObject.cpp:39 network/NetworkDevice.cpp:87
#, kde-format
msgctxt "@title Short for Total Downloaded"
msgid "Downloaded"
msgstr "Descărcat"

#: network/AllDevicesObject.cpp:43 network/NetworkDevice.cpp:91
#, kde-format
msgctxt "@title"
msgid "Total Uploaded"
msgstr "Total încărcat"

#: network/AllDevicesObject.cpp:44 network/NetworkDevice.cpp:92
#, kde-format
msgctxt "@title Short for Total Uploaded"
msgid "Uploaded"
msgstr "Încărcat"

#: network/NetworkDevice.cpp:15
#, kde-format
msgctxt "@title"
msgid "Network Name"
msgstr "Denumire rețea"

#: network/NetworkDevice.cpp:16
#, kde-format
msgctxt "@title Short of Network Name"
msgid "Name"
msgstr "Denumire"

#: network/NetworkDevice.cpp:19
#, kde-format
msgctxt "@title"
msgid "Signal Strength"
msgstr "Intensitate semnal"

#: network/NetworkDevice.cpp:20
#, kde-format
msgctxt "@title Short of Signal Strength"
msgid "Signal"
msgstr "Semnal"

#: network/NetworkDevice.cpp:26
#, kde-format
msgctxt "@title"
msgid "IPv4 Address"
msgstr "Adresă IPv4"

#: network/NetworkDevice.cpp:27
#, kde-format
msgctxt "@title Short of IPv4 Address"
msgid "IPv4"
msgstr "IPv4"

#: network/NetworkDevice.cpp:30
#, kde-format
msgctxt "@title"
msgid "IPv4 Gateway"
msgstr ""

#: network/NetworkDevice.cpp:31
#, kde-format
msgctxt "@title Short of IPv4 Gateway"
msgid "IPv4 Gateway"
msgstr ""

#: network/NetworkDevice.cpp:34
#, kde-format
msgctxt "@title"
msgid "IPv4 Subnet Mask"
msgstr ""

#: network/NetworkDevice.cpp:35
#, kde-format
msgctxt "@title Short of IPv4 Subnet Mask"
msgid "IPv4 Subnet Mask"
msgstr ""

#: network/NetworkDevice.cpp:38
#, kde-format
msgctxt "@title"
msgid "IPv4 with Prefix Length"
msgstr ""

#: network/NetworkDevice.cpp:39
#, fuzzy, kde-format
#| msgctxt "@title Short of IPv4 Address"
#| msgid "IPv4"
msgctxt "@title Short of IPv4 Prefix Length"
msgid "IPv4"
msgstr "IPv4"

#: network/NetworkDevice.cpp:42
#, fuzzy, kde-format
#| msgctxt "@title Short of IPv4 Address"
#| msgid "IPv4"
msgctxt "@title"
msgid "IPv4 DNS"
msgstr "IPv4"

#: network/NetworkDevice.cpp:43
#, fuzzy, kde-format
#| msgctxt "@title Short of IPv4 Address"
#| msgid "IPv4"
msgctxt "@title Short of IPv4 DNS"
msgid "IPv4 DNS"
msgstr "IPv4"

#: network/NetworkDevice.cpp:46
#, kde-format
msgctxt "@title"
msgid "IPv6 Address"
msgstr "Adresă IPv6"

#: network/NetworkDevice.cpp:47
#, kde-format
msgctxt "@title Short of IPv6 Address"
msgid "IPv6"
msgstr "IPv6"

#: network/NetworkDevice.cpp:50
#, kde-format
msgctxt "@title"
msgid "IPv6 Gateway"
msgstr ""

#: network/NetworkDevice.cpp:51
#, kde-format
msgctxt "@title Short of IPv6 Gateway"
msgid "IPv6 Gateway"
msgstr ""

#: network/NetworkDevice.cpp:54
#, kde-format
msgctxt "@title"
msgid "IPv6 Subnet Mask"
msgstr ""

#: network/NetworkDevice.cpp:55
#, kde-format
msgctxt "@title Short of IPv6 Subnet Mask"
msgid "IPv6 Subnet Mask"
msgstr ""

#: network/NetworkDevice.cpp:58
#, kde-format
msgctxt "@title"
msgid "IPv6 with Prefix Length"
msgstr ""

#: network/NetworkDevice.cpp:59
#, fuzzy, kde-format
#| msgctxt "@title Short of IPv6 Address"
#| msgid "IPv6"
msgctxt "@title Short of IPv6 Prefix Length"
msgid "IPv6"
msgstr "IPv6"

#: network/NetworkDevice.cpp:62
#, fuzzy, kde-format
#| msgctxt "@title Short of IPv6 Address"
#| msgid "IPv6"
msgctxt "@title"
msgid "IPv6 DNS"
msgstr "IPv6"

#: network/NetworkDevice.cpp:63
#, fuzzy, kde-format
#| msgctxt "@title Short of IPv6 Address"
#| msgid "IPv6"
msgctxt "@title Short of IPv6 DNS"
msgid "IPv6 DNS"
msgstr "IPv6"

#: network/NetworkPlugin.cpp:43
#, kde-format
msgctxt "@title"
msgid "Network Devices"
msgstr "Dispozitive de rețea"

#: osinfo/osinfo.cpp:106
#, kde-format
msgctxt "@title"
msgid "Operating System"
msgstr "Sistem de operare"

#: osinfo/osinfo.cpp:108
#, kde-format
msgctxt "@title"
msgid "Kernel"
msgstr "Nucleu"

#: osinfo/osinfo.cpp:109
#, kde-format
msgctxt "@title"
msgid "Kernel Name"
msgstr "Denumire nucleu"

#: osinfo/osinfo.cpp:110
#, kde-format
msgctxt "@title"
msgid "Kernel Version"
msgstr "Versiune nucleu"

#: osinfo/osinfo.cpp:111
#, kde-format
msgctxt "@title"
msgid "Kernel Name and Version"
msgstr "Denumire și versiune nucleu"

#: osinfo/osinfo.cpp:112
#, kde-format
msgctxt "@title Kernel Name and Version"
msgid "Kernel"
msgstr "Nucleu"

#: osinfo/osinfo.cpp:114
#, kde-format
msgctxt "@title"
msgid "System"
msgstr "Sistem"

#: osinfo/osinfo.cpp:115
#, kde-format
msgctxt "@title"
msgid "Hostname"
msgstr "Nume gazdă"

#: osinfo/osinfo.cpp:116
#, kde-format
msgctxt "@title"
msgid "Operating System Name"
msgstr "Denumire sistem de operare"

#: osinfo/osinfo.cpp:117
#, kde-format
msgctxt "@title"
msgid "Operating System Version"
msgstr "Versiune sistem de operare"

#: osinfo/osinfo.cpp:118
#, kde-format
msgctxt "@title"
msgid "Operating System Name and Version"
msgstr "Denumire și versiune sistem de operare"

#: osinfo/osinfo.cpp:119
#, kde-format
msgctxt "@title Operating System Name and Version"
msgid "OS"
msgstr "SO"

#: osinfo/osinfo.cpp:120
#, kde-format
msgctxt "@title"
msgid "Operating System Logo"
msgstr "Logotip sistem de operare"

#: osinfo/osinfo.cpp:121
#, kde-format
msgctxt "@title"
msgid "Operating System URL"
msgstr "URL sistem de operare"

#: osinfo/osinfo.cpp:122
#, kde-format
msgctxt "@title"
msgid "Uptime"
msgstr "Durată pornire"

#: osinfo/osinfo.cpp:125
#, kde-format
msgctxt "@title"
msgid "KDE Plasma"
msgstr "Plasma KDE"

#: osinfo/osinfo.cpp:126
#, kde-format
msgctxt "@title"
msgid "Qt Version"
msgstr "Versiune Qt"

#: osinfo/osinfo.cpp:127
#, kde-format
msgctxt "@title"
msgid "KDE Frameworks Version"
msgstr "Versiune KDE Frameworks"

#: osinfo/osinfo.cpp:128
#, kde-format
msgctxt "@title"
msgid "KDE Plasma Version"
msgstr "Versiune KDE Plasma"

#: osinfo/osinfo.cpp:129
#, fuzzy, kde-format
#| msgctxt "@title"
#| msgid "System"
msgctxt "@title"
msgid "Window System"
msgstr "Sistem"

#: osinfo/osinfo.cpp:163
#, kde-format
msgctxt "@info"
msgid "Unknown"
msgstr "Necunoscută"

#: power/power.cpp:44 power/power.cpp:45
#, kde-format
msgctxt "@title"
msgid "Design Capacity"
msgstr "Capacitate proiectată"

#: power/power.cpp:47
#, kde-format
msgid "Amount of energy that the Battery was designed to hold"
msgstr "Cantitatea de energie pe care acumulatorul a fost proiectat s-o țină"

#: power/power.cpp:53 power/power.cpp:54 power/power.cpp:72
#, kde-format
msgctxt "@title"
msgid "Current Capacity"
msgstr "Capacitate actuală"

#: power/power.cpp:56
#, kde-format
msgid "Amount of energy that the battery can currently hold"
msgstr "Cantitatea de energie pe care acumulatorul o poate ține acum"

#: power/power.cpp:62 power/power.cpp:63
#, kde-format
msgctxt "@title"
msgid "Health"
msgstr "Sănătate"

#: power/power.cpp:65
#, kde-format
msgid "Percentage of the design capacity that the battery can hold"
msgstr ""
"Procentajul din capacitatea proiectată pe care acumulatorul poate să-l țină"

#: power/power.cpp:71
#, kde-format
msgctxt "@title"
msgid "Charge"
msgstr "Încărcare"

#: power/power.cpp:74
#, kde-format
msgid "Amount of energy that the battery is currently holding"
msgstr "Cantitatea de energie pe care acumulatorul o ține acum"

#: power/power.cpp:80 power/power.cpp:81
#, kde-format
msgctxt "@title"
msgid "Charge Percentage"
msgstr "Procentaj încărcare"

#: power/power.cpp:83
#, kde-format
msgid ""
"Percentage of the current capacity that the battery is currently holding"
msgstr "Procentajul din capacitatea actuală pe care acumulatorul îl ține acum"

#: power/power.cpp:90
#, kde-format
msgctxt "@title"
msgid "Charging Rate"
msgstr "Rată de încărcare"

#: power/power.cpp:91
#, kde-format
msgctxt "@title"
msgid "Charging  Rate"
msgstr "Rată de încărcare"

#: power/power.cpp:93
#, kde-format
msgid ""
"Power that the battery is being charged with (positive) or discharged "
"(negative)"
msgstr ""
"Puterea cu care e încărcat (pozitiv) sau descărcat (negativ) acumulatorul"

#, fuzzy
#~| msgctxt "@title, Short for Current Temperatur"
#~| msgid "Temperature"
#~ msgctxt "@title %1 is a number"
#~ msgid "Temperature %1"
#~ msgstr "Temperatură"

#~ msgctxt "@info"
#~ msgid "Percentage of memory taken by applications."
#~ msgstr "Procentajul de memorie ocupată de aplicații."

#~ msgctxt "@info"
#~ msgid "Percentage of memory taken by the buffer."
#~ msgstr "Procentajul de memorie ocupată de tampon."

#~ msgctxt "@info"
#~ msgid "Percentage of memory taken by the cache."
#~ msgstr "Procentajul de memorie ocupată de cache."

#~ msgctxt "@title Free Memory Percentage"
#~ msgid "Free"
#~ msgstr "Liberă"

#~ msgctxt "@info"
#~ msgid "Percentage of free memory."
#~ msgstr "Procentajul de memorie liberă."

#~ msgctxt "@title Used Memory Percentage"
#~ msgid "Used"
#~ msgstr "Folosită"

#~ msgctxt "@info"
#~ msgid "Percentage of used memory."
#~ msgstr "Procentajul de memorie folosită."

#~ msgctxt "@title"
#~ msgid "Available Memory Percentage"
#~ msgstr "Procentaj memorie disponibilă"

#~ msgctxt "@title Available Memory Percentage"
#~ msgid "Available"
#~ msgstr "Disponibilă"

#~ msgctxt "@info"
#~ msgid "Percentage of available memory."
#~ msgstr "Procentajul de memorie disponibilă."

#~ msgctxt "@title"
#~ msgid "Allocated Memory Percentage"
#~ msgstr "Procentaj memorie alocată"

#~ msgctxt "@title Allocated Memory Percentage"
#~ msgid "Allocated"
#~ msgstr "Alocată"

#~ msgctxt "@info"
#~ msgid "Percentage of allocated memory."
#~ msgstr "Procentajul de memorie alocată."

#~ msgctxt "@title Total CPU Usage"
#~ msgid "Usage"
#~ msgstr "Utilizare"

#~ msgctxt "@title Total Memory Usage"
#~ msgid "Total Used"
#~ msgstr "Total folosită"

#~ msgctxt "@title Cached Memory Usage"
#~ msgid "Cached"
#~ msgstr "În cache"

#~ msgctxt "@title Free Memory Amount"
#~ msgid "Free"
#~ msgstr "Liberă"

#~ msgctxt "@title Available Memory Amount"
#~ msgid "Available"
#~ msgstr "Disponibilă"

#~ msgctxt "@title Application Memory Usage"
#~ msgid "Application"
#~ msgstr "Aplicație"

#~ msgctxt "@title Buffer Memory Usage"
#~ msgid "Buffer"
#~ msgstr "Tampon"

#~ msgctxt "@title Number of Processors"
#~ msgid "Processors"
#~ msgstr "Procesoare"

#~ msgctxt "@title Number of Cores"
#~ msgid "Cores"
#~ msgstr "Nuclee"

#~ msgctxt "@title"
#~ msgid "GPU %1 Power Usage"
#~ msgstr "Consum energie GPU %1"

#~ msgctxt "@title GPU Power Usage"
#~ msgid "Power"
#~ msgstr "Energie"

#~ msgctxt "@title GPU Temperature"
#~ msgid "Temperature"
#~ msgstr "Temperatură"

#~ msgctxt "@title"
#~ msgid "GPU %1 Shared Memory Usage"
#~ msgstr "Utilizare memorie partajată GPU %1"

#~ msgctxt "@title"
#~ msgid "GPU %1 Encoder Usage"
#~ msgstr "Utilizare codor GPU %1"

#~ msgctxt "@title GPU Encoder Usage"
#~ msgid "Encoder"
#~ msgstr "Codor"

#~ msgctxt "@title"
#~ msgid "GPU %1 Decoder Usage"
#~ msgstr "Utilizare decodor GPU %1"

#~ msgctxt "@title GPU Decoder Usage"
#~ msgid "Decoder"
#~ msgstr "Decodor"

#~ msgctxt "@title"
#~ msgid "GPU %1 Memory Clock"
#~ msgstr "Frecvență memorie GPU %1"

#~ msgctxt "@title GPU Memory Clock"
#~ msgid "Memory Clock"
#~ msgstr "Frecvență memorie"

#~ msgctxt "@title"
#~ msgid "GPU %1 Processor Clock"
#~ msgstr "Frecvență procesor GPU %1"

#~ msgctxt "@title GPU Processor Clock"
#~ msgid "Processor Clock"
#~ msgstr "Frecvență procesor"

#~ msgctxt "@title NVidia GPU information"
#~ msgid "NVidia"
#~ msgstr "NVidia"

#~ msgctxt "@title"
#~ msgid "Disk Read Accesses"
#~ msgstr "Accesări de citire disc"

#~ msgctxt "@info"
#~ msgid "Read accesses across all disk devices"
#~ msgstr "Accesări de citire pe toate dispozitivele de disc"

#~ msgctxt "@title"
#~ msgid "Disk Write Accesses"
#~ msgstr "Accesări de scriere disc"

#~ msgctxt "@info"
#~ msgid "Write accesses across all disk devices"
#~ msgstr "Accesări de scriere pe toate dispozitivele de disc"
