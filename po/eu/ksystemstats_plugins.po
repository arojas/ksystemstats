# Translation for ksysguard_plugins_global.po to Euskara/Basque (eu).
# Copyright (C) 2020-2022, This file is copyright:
# This file is distributed under the same license as the ksysguard package.
# KDE euskaratzeko proiektuko arduraduna <xalba@ni.eus>.
#
# Translators:
# Iñigo Salvador Azurmendi <xalba@ni.eus>, 2020, 2022.
msgid ""
msgstr ""
"Project-Id-Version: ksystemstats\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-27 01:38+0000\n"
"PO-Revision-Date: 2022-10-01 19:01+0200\n"
"Last-Translator: Iñigo Salvador Azurmendi <xalba@ni.eus>\n"
"Language-Team: Basque <kde-i18n-eu@kde.org>\n"
"Language: eu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 22.08.1\n"

#: cpu/cpu.cpp:28 disks/disks.cpp:74 disks/disks.cpp:75 gpu/GpuDevice.cpp:20
#: power/power.cpp:39
#, kde-format
msgctxt "@title"
msgid "Name"
msgstr "Izena"

#: cpu/cpu.cpp:38
#, kde-format
msgctxt "@title"
msgid "Total Usage"
msgstr "Erabilitako guztizkoa"

#: cpu/cpu.cpp:39
#, kde-format
msgctxt "@title, Short for 'Total Usage'"
msgid "Usage"
msgstr "Erabilera"

#: cpu/cpu.cpp:45
#, kde-format
msgctxt "@title"
msgid "System Usage"
msgstr "Sistemaren erabilera"

#: cpu/cpu.cpp:46
#, kde-format
msgctxt "@title, Short for 'System Usage'"
msgid "System"
msgstr "Sistema"

#: cpu/cpu.cpp:52
#, kde-format
msgctxt "@title"
msgid "User Usage"
msgstr "Erabiltzailearen erabilera"

#: cpu/cpu.cpp:53
#, kde-format
msgctxt "@title, Short for 'User Usage'"
msgid "User"
msgstr "Erabiltzailea"

#: cpu/cpu.cpp:59
#, kde-format
msgctxt "@title"
msgid "Wait Usage"
msgstr "Itxoiteko erabilera"

#: cpu/cpu.cpp:60
#, kde-format
msgctxt "@title, Short for 'Wait Load'"
msgid "Wait"
msgstr "Itxoin"

#: cpu/cpu.cpp:85
#, kde-format
msgctxt "@title"
msgid "Current Frequency"
msgstr "Uneko maiztasuna"

#: cpu/cpu.cpp:86
#, kde-format
msgctxt "@title, Short for 'Current Frequency'"
msgid "Frequency"
msgstr "Maiztasuna"

#: cpu/cpu.cpp:87
#, kde-format
msgctxt "@info"
msgid "Current frequency of the CPU"
msgstr "PUZaren uneko maiztasuna"

#: cpu/cpu.cpp:92
#, kde-format
msgctxt "@title"
msgid "Current Temperature"
msgstr "Uneko tenperatura"

#: cpu/cpu.cpp:93
#, kde-format
msgctxt "@title, Short for Current Temperatur"
msgid "Temperature"
msgstr "Tenperatura"

#: cpu/cpu.cpp:100
#, kde-format
msgctxt "@title"
msgid "All"
msgstr "Guztiak"

#: cpu/cpu.cpp:119
#, kde-format
msgctxt "@title"
msgid "Maximum CPU Frequency"
msgstr "PUZaren gehienezko maiztasuna"

#: cpu/cpu.cpp:120
#, kde-format
msgctxt "@title, Short for 'Maximum CPU Frequency'"
msgid "Max Frequency"
msgstr "Gehienezko maiztasuna"

#: cpu/cpu.cpp:121
#, kde-format
msgctxt "@info"
msgid "Current maximum frequency between all CPUs"
msgstr "Uneko gehienezko maiztasuna PUZ guztien artean"

#: cpu/cpu.cpp:126
#, kde-format
msgctxt "@title"
msgid "Minimum CPU Frequency"
msgstr "PUZaren gutxieneko maiztasuna"

#: cpu/cpu.cpp:127
#, kde-format
msgctxt "@title, Short for 'Minimum CPU Frequency'"
msgid "Min Frequency"
msgstr "Gutxieneko maiztasuna"

#: cpu/cpu.cpp:128
#, kde-format
msgctxt "@info"
msgid "Current minimum frequency between all CPUs"
msgstr "Uneko gutxieneko maiztasuna PUZ guztien artean"

#: cpu/cpu.cpp:133
#, kde-format
msgctxt "@title"
msgid "Average CPU Frequency"
msgstr "PUZaren batezbestekoko maiztasuna"

#: cpu/cpu.cpp:134
#, kde-format
msgctxt "@title, Short for 'Average CPU Frequency'"
msgid "Average Frequency"
msgstr "Batezbesteko maiztasuna"

#: cpu/cpu.cpp:135
#, kde-format
msgctxt "@info"
msgid "Current average frequency between all CPUs"
msgstr "Uneko batezbesteko maiztasuna PUZ guztien artean"

#: cpu/cpu.cpp:140
#, kde-format
msgctxt "@title"
msgid "Maximum CPU Temperature"
msgstr "PUZaren gehienezko tenperatura"

#: cpu/cpu.cpp:141
#, kde-format
msgctxt "@title, Short for 'Maximum CPU Temperature'"
msgid "Max Temperature"
msgstr "Gehieneko tenperatura"

#: cpu/cpu.cpp:147
#, kde-format
msgctxt "@title"
msgid "Minimum CPU Temperature"
msgstr "PUZaren gutxieneko tenperatura"

#: cpu/cpu.cpp:148
#, kde-format
msgctxt "@title, Short for 'Minimum CPU Temperature'"
msgid "Min Temperature"
msgstr "Gutxieneko tenperatura"

#: cpu/cpu.cpp:154
#, kde-format
msgctxt "@title"
msgid "Average CPU Temperature"
msgstr "PUZ batezbesteko tenperatura"

#: cpu/cpu.cpp:155
#, kde-format
msgctxt "@title, Short for 'Average CPU Temperature'"
msgid "Average Temperature"
msgstr "Batezbesteko tenperatura"

#: cpu/cpu.cpp:171
#, kde-format
msgctxt "@title"
msgid "Number of CPUs"
msgstr "PUZ kopurua"

#: cpu/cpu.cpp:172
#, kde-format
msgctxt "@title, Short fort 'Number of CPUs'"
msgid "CPUs"
msgstr "PUZak"

#: cpu/cpu.cpp:173
#, kde-format
msgctxt "@info"
msgid "Number of physical CPUs installed in the system"
msgstr "Sisteman zenbat PUZ fisiko instalatuta dauden"

#: cpu/cpu.cpp:175
#, kde-format
msgctxt "@title"
msgid "Number of Cores"
msgstr "Nukleo kopurua"

#: cpu/cpu.cpp:176
#, kde-format
msgctxt "@title, Short fort 'Number of Cores'"
msgid "Cores"
msgstr "Nukleoak"

#: cpu/cpu.cpp:177
#, kde-format
msgctxt "@info"
msgid "Number of CPU cores across all physical CPUS"
msgstr "PUZ fisiko guztien artean dauden nukleoak"

#: cpu/cpuplugin.cpp:19
#, kde-format
msgid "CPUs"
msgstr "PUZak"

#: cpu/freebsdcpuplugin.cpp:151
#, kde-format
msgctxt "@title"
msgid "CPU %1"
msgstr "%1. PUZ"

#: cpu/linuxcpuplugin.cpp:39
#, kde-format
msgctxt "@title"
msgid "Core %1"
msgstr "%1 nukleoa"

#: cpu/linuxcpuplugin.cpp:54
#, kde-format
msgctxt "@title"
msgid "CPU %1 Core %2"
msgstr "%1. PUZ %2. nukleoa"

#: cpu/loadaverages.cpp:13
#, kde-format
msgctxt "@title"
msgid "Load Averages"
msgstr "Batezbesteko zamak"

#: cpu/loadaverages.cpp:14
#, kde-format
msgctxt "@title"
msgid "Load average (1 minute)"
msgstr "Batezbesteko zama (minutu 1)"

#: cpu/loadaverages.cpp:15
#, kde-format
msgctxt "@title"
msgid "Load average (5 minutes)"
msgstr "Batezbesteko zama (5 minutu)"

#: cpu/loadaverages.cpp:16
#, kde-format
msgctxt "@title"
msgid "Load average (15 minute)"
msgstr "Batezbesteko zama (15 minutu)"

#: cpu/loadaverages.cpp:18
#, kde-format
msgctxt "@title,  Short for 'Load average (1 minute)"
msgid "Load average (1m)"
msgstr "Batezbesteko zama (1 m)"

#: cpu/loadaverages.cpp:19
#, kde-format
msgctxt "@title,  Short for 'Load average (5 minutes)"
msgid "Load average (5m)"
msgstr "Batezbesteko zama (5 m)"

#: cpu/loadaverages.cpp:20
#, kde-format
msgctxt "@title,  Short for 'Load average (15 minutes)"
msgid "Load average (15m)"
msgstr "Batezbesteko zama (15 m)"

#: cpu/loadaverages.cpp:22
#, kde-format
msgctxt "@info"
msgid "Number of jobs in the run queue averaged over 1 minute"
msgstr "Exekuzio ilarako ataza kopurua, minutu 1eko batezbestekoa"

#: cpu/loadaverages.cpp:23
#, kde-format
msgctxt "@info"
msgid "Number of jobs in the run queue averaged over 5 minutes"
msgstr "Exekuzio ilarako ataza kopurua, 5 minutuko batezbestekoa"

#: cpu/loadaverages.cpp:24
#, kde-format
msgctxt "@info"
msgid "Number of jobs in the run queue averaged over 15 minutes"
msgstr "Exekuzio ilarako ataza kopurua, 15 minutuko batezbestekoa"

#: disks/disks.cpp:78 disks/disks.cpp:258
#, kde-format
msgctxt "@title"
msgid "Total Space"
msgstr "Espazioa guztira"

#: disks/disks.cpp:80 disks/disks.cpp:259
#, kde-format
msgctxt "@title Short for 'Total Space'"
msgid "Total"
msgstr "Guztira"

#: disks/disks.cpp:85 disks/disks.cpp:271
#, kde-format
msgctxt "@title"
msgid "Used Space"
msgstr "Erabilitako espazioa"

#: disks/disks.cpp:87 disks/disks.cpp:272
#, kde-format
msgctxt "@title Short for 'Used Space'"
msgid "Used"
msgstr "Erabilita"

#: disks/disks.cpp:92 disks/disks.cpp:264
#, kde-format
msgctxt "@title"
msgid "Free Space"
msgstr "Espazio hutsa"

#: disks/disks.cpp:94 disks/disks.cpp:265
#, kde-format
msgctxt "@title Short for 'Free Space'"
msgid "Free"
msgstr "Hutsik"

#: disks/disks.cpp:100 disks/disks.cpp:278
#, kde-format
msgctxt "@title"
msgid "Read Rate"
msgstr "Irakurtze-emaria"

#: disks/disks.cpp:102 disks/disks.cpp:279
#, kde-format
msgctxt "@title Short for 'Read Rate'"
msgid "Read"
msgstr "Irakurtzea"

#: disks/disks.cpp:106 disks/disks.cpp:284
#, kde-format
msgctxt "@title"
msgid "Write Rate"
msgstr "Idazte-emaria"

#: disks/disks.cpp:108 disks/disks.cpp:285
#, kde-format
msgctxt "@title Short for 'Write Rate'"
msgid "Write"
msgstr "Idaztea"

#: disks/disks.cpp:113 disks/disks.cpp:294
#, kde-format
msgctxt "@title"
msgid "Percentage Used"
msgstr "Erabilitako ehunekoa"

#: disks/disks.cpp:117 disks/disks.cpp:290
#, kde-format
msgctxt "@title"
msgid "Percentage Free"
msgstr "Hutsik dagoen ehunekoa"

#: disks/disks.cpp:157
#, kde-format
msgid "Disks"
msgstr "Diskoak"

#: disks/disks.cpp:256
#, kde-format
msgctxt "@title"
msgid "All Disks"
msgstr "Disko guztiak"

#: disks/disks.cpp:291
#, kde-format
msgctxt "@title, Short for `Percentage Free"
msgid "Free"
msgstr "Hutsik"

#: disks/disks.cpp:295
#, kde-format
msgctxt "@title, Short for `Percentage Used"
msgid "Used"
msgstr "Erabilita"

#: gpu/AllGpus.cpp:14
#, kde-format
msgctxt "@title"
msgid "All GPUs"
msgstr "GPU guztiak"

#: gpu/AllGpus.cpp:16
#, kde-format
msgctxt "@title"
msgid "All GPUs Usage"
msgstr "GPU guztien erabilera"

#: gpu/AllGpus.cpp:17
#, kde-format
msgctxt "@title Short for 'All GPUs Usage'"
msgid "Usage"
msgstr "Erabilera"

#: gpu/AllGpus.cpp:26
#, kde-format
msgctxt "@title"
msgid "All GPUs Total Memory"
msgstr "GPU guztien memoria osoa"

#: gpu/AllGpus.cpp:27
#, kde-format
msgctxt "@title Short for 'All GPUs Total Memory'"
msgid "Total"
msgstr "Guztira"

#: gpu/AllGpus.cpp:31
#, kde-format
msgctxt "@title"
msgid "All GPUs Used Memory"
msgstr "GPU guztiek erabilitako memoria"

#: gpu/AllGpus.cpp:32
#, kde-format
msgctxt "@title Short for 'All GPUs Used Memory'"
msgid "Used"
msgstr "Erabilita"

#: gpu/GpuDevice.cpp:24
#, kde-format
msgctxt "@title"
msgid "Usage"
msgstr "Erabilera"

#: gpu/GpuDevice.cpp:30
#, kde-format
msgctxt "@title"
msgid "Total Video Memory"
msgstr "Bideo memoria osoa"

#: gpu/GpuDevice.cpp:32
#, kde-format
msgctxt "@title Short for Total Video Memory"
msgid "Total"
msgstr "Guztira"

#: gpu/GpuDevice.cpp:35
#, kde-format
msgctxt "@title"
msgid "Video Memory Used"
msgstr "Erabilitako bideo memoria"

#: gpu/GpuDevice.cpp:37
#, kde-format
msgctxt "@title Short for Video Memory Used"
msgid "Used"
msgstr "Erabilita"

#: gpu/GpuDevice.cpp:41
#, kde-format
msgctxt "@title"
msgid "Frequency"
msgstr "Maiztasuna"

#: gpu/GpuDevice.cpp:45
#, kde-format
msgctxt "@title"
msgid "Memory Frequency"
msgstr "Memoriaren maiztasuna"

#: gpu/GpuDevice.cpp:49
#, kde-format
msgctxt "@title"
msgid "Temperature"
msgstr "Tenperatura"

#: gpu/GpuDevice.cpp:53 power/power.cpp:106
#, kde-format
msgctxt "@title"
msgid "Power"
msgstr "Energia"

#: gpu/GpuPlugin.cpp:31
#, kde-format
msgctxt "@title"
msgid "GPU"
msgstr "GPU"

#: gpu/LinuxBackend.cpp:54
#, kde-format
msgctxt "@title %1 is GPU number"
msgid "GPU %1"
msgstr "%1. GPU"

#: lmsensors/lmsensors.cpp:22
#, kde-format
msgid "Hardware Sensors"
msgstr "Hardware sentsoreak"

#: memory/backend.cpp:17
#, kde-format
msgctxt "@title"
msgid "Physical Memory"
msgstr "Memoria fisikoa"

#: memory/backend.cpp:18
#, kde-format
msgctxt "@title"
msgid "Swap Memory"
msgstr "Trukatze-memoria"

#: memory/backend.cpp:39
#, kde-format
msgctxt "@title"
msgid "Total Physical Memory"
msgstr "Memoria fisiko osoa"

#: memory/backend.cpp:40
#, kde-format
msgctxt "@title, Short for 'Total Physical Memory'"
msgid "Total"
msgstr "Guztira"

#: memory/backend.cpp:44
#, kde-format
msgctxt "@title"
msgid "Used Physical Memory"
msgstr "Erabilitako memoria fisikoa"

#: memory/backend.cpp:45
#, kde-format
msgctxt "@title, Short for 'Used Physical Memory'"
msgid "Used"
msgstr "Erabilita"

#: memory/backend.cpp:49
#, kde-format
msgctxt "@title"
msgid "Used Physical Memory Percentage"
msgstr "Erabilitako memoria fisikoaren ehunekoa"

#: memory/backend.cpp:53
#, kde-format
msgctxt "@title"
msgid "Free Physical Memory"
msgstr "Memoria fisiko hutsik"

#: memory/backend.cpp:54
#, kde-format
msgctxt "@title, Short for 'Free Physical Memory'"
msgid "Free"
msgstr "Hutsik"

#: memory/backend.cpp:58
#, kde-format
msgctxt "@title"
msgid "Free Physical Memory Percentage"
msgstr "Memoria fisiko hutsaren ehunekoa"

#: memory/backend.cpp:62
#, kde-format
msgctxt "@title"
msgid "Application Memory"
msgstr "Aplikazioen memoria"

#: memory/backend.cpp:63
#, kde-format
msgctxt "@title, Short for 'Application Memory'"
msgid "Application"
msgstr "Aplikazioa"

#: memory/backend.cpp:67
#, kde-format
msgctxt "@title"
msgid "Application Memory Percentage"
msgstr "Aplikazioen memoriaren ehunekoa"

#: memory/backend.cpp:71
#, kde-format
msgctxt "@title"
msgid "Cache Memory"
msgstr "Cache memoria"

#: memory/backend.cpp:72
#, kde-format
msgctxt "@title, Short for 'Cache Memory'"
msgid "Cache"
msgstr "Cachea"

#: memory/backend.cpp:76
#, kde-format
msgctxt "@title"
msgid "Cache Memory Percentage"
msgstr "Cache memoriaren ehunekoa"

#: memory/backend.cpp:80
#, kde-format
msgctxt "@title"
msgid "Buffer Memory"
msgstr "Tarteko-memoria"

#: memory/backend.cpp:81
#, kde-format
msgctxt "@title, Short for 'Buffer Memory'"
msgid "Buffer"
msgstr "Tarteko-memoria"

#: memory/backend.cpp:82
#, kde-format
msgid "Amount of memory used for caching disk blocks"
msgstr "Diskoko blokeak cacheratzeko erabilitako memoria kopurua"

#: memory/backend.cpp:86
#, kde-format
msgctxt "@title"
msgid "Buffer Memory Percentage"
msgstr "Tarteko-memoriaren ehunekoa"

#: memory/backend.cpp:90
#, kde-format
msgctxt "@title"
msgid "Total Swap Memory"
msgstr "Trukatze-memoria osoa"

#: memory/backend.cpp:91
#, kde-format
msgctxt "@title, Short for 'Total Swap Memory'"
msgid "Total"
msgstr "Guztira"

#: memory/backend.cpp:95
#, kde-format
msgctxt "@title"
msgid "Used Swap Memory"
msgstr "Erabilitako trukatze-memoria"

#: memory/backend.cpp:96
#, kde-format
msgctxt "@title, Short for 'Used Swap Memory'"
msgid "Used"
msgstr "Erabilita"

#: memory/backend.cpp:100
#, kde-format
msgctxt "@title"
msgid "Used Swap Memory Percentage"
msgstr "Erabilitako trukatze-memoriaren ehunekoa"

#: memory/backend.cpp:104
#, kde-format
msgctxt "@title"
msgid "Free Swap Memory"
msgstr "Trukatze-memoria hutsik"

#: memory/backend.cpp:105
#, kde-format
msgctxt "@title, Short for 'Free Swap Memory'"
msgid "Free"
msgstr "Hutsik"

#: memory/backend.cpp:109
#, kde-format
msgctxt "@title"
msgid "Free Swap Memory Percentage"
msgstr "Trukatze-memoria hutsaren ehunekoa"

#: memory/memory.cpp:24
#, kde-format
msgctxt "@title"
msgid "Memory"
msgstr "Memoria"

#: network/AllDevicesObject.cpp:16
#, kde-format
msgctxt "@title"
msgid "All Network Devices"
msgstr "Sareko gailu guztiak"

#: network/AllDevicesObject.cpp:18 network/AllDevicesObject.cpp:28
#: network/NetworkDevice.cpp:66 network/NetworkDevice.cpp:76
#, kde-format
msgctxt "@title"
msgid "Download Rate"
msgstr "Zama-jaiste emaria"

#: network/AllDevicesObject.cpp:19 network/AllDevicesObject.cpp:29
#: network/NetworkDevice.cpp:67 network/NetworkDevice.cpp:77
#, kde-format
msgctxt "@title Short for Download Rate"
msgid "Download"
msgstr "Zama-jaistea"

#: network/AllDevicesObject.cpp:23 network/AllDevicesObject.cpp:33
#: network/NetworkDevice.cpp:71 network/NetworkDevice.cpp:81
#, kde-format
msgctxt "@title"
msgid "Upload Rate"
msgstr "Zama-igotze emaria"

#: network/AllDevicesObject.cpp:24 network/AllDevicesObject.cpp:34
#: network/NetworkDevice.cpp:72 network/NetworkDevice.cpp:82
#, kde-format
msgctxt "@title Short for Upload Rate"
msgid "Upload"
msgstr "Zama-igotzea"

#: network/AllDevicesObject.cpp:38 network/NetworkDevice.cpp:86
#, kde-format
msgctxt "@title"
msgid "Total Downloaded"
msgstr "Zama-jaitsitako guztizkoa"

#: network/AllDevicesObject.cpp:39 network/NetworkDevice.cpp:87
#, kde-format
msgctxt "@title Short for Total Downloaded"
msgid "Downloaded"
msgstr "Zama-jaitsita"

#: network/AllDevicesObject.cpp:43 network/NetworkDevice.cpp:91
#, kde-format
msgctxt "@title"
msgid "Total Uploaded"
msgstr "Zama-igotako guztizkoa"

#: network/AllDevicesObject.cpp:44 network/NetworkDevice.cpp:92
#, kde-format
msgctxt "@title Short for Total Uploaded"
msgid "Uploaded"
msgstr "Zama-igota"

#: network/NetworkDevice.cpp:15
#, kde-format
msgctxt "@title"
msgid "Network Name"
msgstr "Sare-izena"

#: network/NetworkDevice.cpp:16
#, kde-format
msgctxt "@title Short of Network Name"
msgid "Name"
msgstr "Izena"

#: network/NetworkDevice.cpp:19
#, kde-format
msgctxt "@title"
msgid "Signal Strength"
msgstr "Seinalearen indarra"

#: network/NetworkDevice.cpp:20
#, kde-format
msgctxt "@title Short of Signal Strength"
msgid "Signal"
msgstr "Seinalea"

#: network/NetworkDevice.cpp:26
#, kde-format
msgctxt "@title"
msgid "IPv4 Address"
msgstr "IPv4 helbidea"

#: network/NetworkDevice.cpp:27
#, kde-format
msgctxt "@title Short of IPv4 Address"
msgid "IPv4"
msgstr "IPv4"

#: network/NetworkDevice.cpp:30
#, kde-format
msgctxt "@title"
msgid "IPv4 Gateway"
msgstr "IPv4 atebidea"

#: network/NetworkDevice.cpp:31
#, kde-format
msgctxt "@title Short of IPv4 Gateway"
msgid "IPv4 Gateway"
msgstr "IPv4 atebidea"

#: network/NetworkDevice.cpp:34
#, kde-format
msgctxt "@title"
msgid "IPv4 Subnet Mask"
msgstr "IPv4 azpisare-maskara"

#: network/NetworkDevice.cpp:35
#, kde-format
msgctxt "@title Short of IPv4 Subnet Mask"
msgid "IPv4 Subnet Mask"
msgstr "IPv4 azpisare-maskara"

#: network/NetworkDevice.cpp:38
#, kde-format
msgctxt "@title"
msgid "IPv4 with Prefix Length"
msgstr "IPv4 aurrizki luzerarekin"

#: network/NetworkDevice.cpp:39
#, kde-format
msgctxt "@title Short of IPv4 Prefix Length"
msgid "IPv4"
msgstr "IPv4"

#: network/NetworkDevice.cpp:42
#, kde-format
msgctxt "@title"
msgid "IPv4 DNS"
msgstr "IPv4 DNS"

#: network/NetworkDevice.cpp:43
#, kde-format
msgctxt "@title Short of IPv4 DNS"
msgid "IPv4 DNS"
msgstr "IPv4 DNS"

#: network/NetworkDevice.cpp:46
#, kde-format
msgctxt "@title"
msgid "IPv6 Address"
msgstr "IPv6 helbidea"

#: network/NetworkDevice.cpp:47
#, kde-format
msgctxt "@title Short of IPv6 Address"
msgid "IPv6"
msgstr "IPv6"

#: network/NetworkDevice.cpp:50
#, kde-format
msgctxt "@title"
msgid "IPv6 Gateway"
msgstr "IPv6 atebidea"

#: network/NetworkDevice.cpp:51
#, kde-format
msgctxt "@title Short of IPv6 Gateway"
msgid "IPv6 Gateway"
msgstr "IPv6 atebidea"

#: network/NetworkDevice.cpp:54
#, kde-format
msgctxt "@title"
msgid "IPv6 Subnet Mask"
msgstr "IPv6 azpisare-maskara"

#: network/NetworkDevice.cpp:55
#, kde-format
msgctxt "@title Short of IPv6 Subnet Mask"
msgid "IPv6 Subnet Mask"
msgstr "IPv6 azpisare-maskara"

#: network/NetworkDevice.cpp:58
#, kde-format
msgctxt "@title"
msgid "IPv6 with Prefix Length"
msgstr "IPv6 aurrizki luzerarekin"

#: network/NetworkDevice.cpp:59
#, kde-format
msgctxt "@title Short of IPv6 Prefix Length"
msgid "IPv6"
msgstr "IPv6"

#: network/NetworkDevice.cpp:62
#, kde-format
msgctxt "@title"
msgid "IPv6 DNS"
msgstr "IPv6 DNS"

#: network/NetworkDevice.cpp:63
#, kde-format
msgctxt "@title Short of IPv6 DNS"
msgid "IPv6 DNS"
msgstr "IPv6 DNS"

#: network/NetworkPlugin.cpp:43
#, kde-format
msgctxt "@title"
msgid "Network Devices"
msgstr "Sareko gailuak"

#: osinfo/osinfo.cpp:106
#, kde-format
msgctxt "@title"
msgid "Operating System"
msgstr "Sistema eragilea"

#: osinfo/osinfo.cpp:108
#, kde-format
msgctxt "@title"
msgid "Kernel"
msgstr "Muina"

#: osinfo/osinfo.cpp:109
#, kde-format
msgctxt "@title"
msgid "Kernel Name"
msgstr "Muinaren izena"

#: osinfo/osinfo.cpp:110
#, kde-format
msgctxt "@title"
msgid "Kernel Version"
msgstr "Munaren bertsioa"

#: osinfo/osinfo.cpp:111
#, kde-format
msgctxt "@title"
msgid "Kernel Name and Version"
msgstr "Munaren izena eta bertsioa"

#: osinfo/osinfo.cpp:112
#, kde-format
msgctxt "@title Kernel Name and Version"
msgid "Kernel"
msgstr "Muina"

#: osinfo/osinfo.cpp:114
#, kde-format
msgctxt "@title"
msgid "System"
msgstr "Sistema"

#: osinfo/osinfo.cpp:115
#, kde-format
msgctxt "@title"
msgid "Hostname"
msgstr "Ostalari-izena"

#: osinfo/osinfo.cpp:116
#, kde-format
msgctxt "@title"
msgid "Operating System Name"
msgstr "Sistema eragilearen izena"

#: osinfo/osinfo.cpp:117
#, kde-format
msgctxt "@title"
msgid "Operating System Version"
msgstr "Sistema eragilearen bertsioa"

#: osinfo/osinfo.cpp:118
#, kde-format
msgctxt "@title"
msgid "Operating System Name and Version"
msgstr "Sistema eragilearen izena eta bertsioa"

#: osinfo/osinfo.cpp:119
#, kde-format
msgctxt "@title Operating System Name and Version"
msgid "OS"
msgstr "SE"

#: osinfo/osinfo.cpp:120
#, kde-format
msgctxt "@title"
msgid "Operating System Logo"
msgstr "Sistema eragilearen logotipoa"

#: osinfo/osinfo.cpp:121
#, kde-format
msgctxt "@title"
msgid "Operating System URL"
msgstr "Sistema eragilearen URLa"

#: osinfo/osinfo.cpp:122
#, kde-format
msgctxt "@title"
msgid "Uptime"
msgstr "Jardun-denbora"

#: osinfo/osinfo.cpp:125
#, kde-format
msgctxt "@title"
msgid "KDE Plasma"
msgstr "KDE Plasma"

#: osinfo/osinfo.cpp:126
#, kde-format
msgctxt "@title"
msgid "Qt Version"
msgstr "Qt bertsioa"

#: osinfo/osinfo.cpp:127
#, kde-format
msgctxt "@title"
msgid "KDE Frameworks Version"
msgstr "KDE Frameworks-en bertsioa"

#: osinfo/osinfo.cpp:128
#, kde-format
msgctxt "@title"
msgid "KDE Plasma Version"
msgstr "KDE Plasmaren bertsioa"

#: osinfo/osinfo.cpp:129
#, kde-format
msgctxt "@title"
msgid "Window System"
msgstr "Leiho-sistema"

#: osinfo/osinfo.cpp:163
#, kde-format
msgctxt "@info"
msgid "Unknown"
msgstr "Ezezaguna"

#: power/power.cpp:44 power/power.cpp:45
#, kde-format
msgctxt "@title"
msgid "Design Capacity"
msgstr "Diseinuzko kapazitatea"

#: power/power.cpp:47
#, kde-format
msgid "Amount of energy that the Battery was designed to hold"
msgstr "Bateria zein energia kopuru eusteko diseinatu zen"

#: power/power.cpp:53 power/power.cpp:54 power/power.cpp:72
#, kde-format
msgctxt "@title"
msgid "Current Capacity"
msgstr "Uneko kapazitatea"

#: power/power.cpp:56
#, kde-format
msgid "Amount of energy that the battery can currently hold"
msgstr "Bateriak gaur egun eutsi dezakeen energia"

#: power/power.cpp:62 power/power.cpp:63
#, kde-format
msgctxt "@title"
msgid "Health"
msgstr "Osasuna"

#: power/power.cpp:65
#, kde-format
msgid "Percentage of the design capacity that the battery can hold"
msgstr "Bateriak eutsi dezakeen diseinuzko kapazitatearen ehunekoa"

#: power/power.cpp:71
#, kde-format
msgctxt "@title"
msgid "Charge"
msgstr "Karga"

#: power/power.cpp:74
#, kde-format
msgid "Amount of energy that the battery is currently holding"
msgstr "Bateria une honetan eusten ari den energia kopurua"

#: power/power.cpp:80 power/power.cpp:81
#, kde-format
msgctxt "@title"
msgid "Charge Percentage"
msgstr "Kargaren ehunekoa"

#: power/power.cpp:83
#, kde-format
msgid ""
"Percentage of the current capacity that the battery is currently holding"
msgstr "Bateria une honetan eusten ari den gaur egungo kapazitatearen ehunekoa"

#: power/power.cpp:90
#, kde-format
msgctxt "@title"
msgid "Charging Rate"
msgstr "Kargatzeko abiadura"

#: power/power.cpp:91
#, kde-format
msgctxt "@title"
msgid "Charging  Rate"
msgstr "Kargatzeko abiadura"

#: power/power.cpp:93
#, kde-format
msgid ""
"Power that the battery is being charged with (positive) or discharged "
"(negative)"
msgstr ""
"Bateria kargatzen (positiboa) edo deskargatzen (negatiboa) ari den potentzia"

#, fuzzy
#~| msgctxt "@title, Short for Current Temperatur"
#~| msgid "Temperature"
#~ msgctxt "@title %1 is a number"
#~ msgid "Temperature %1"
#~ msgstr "Tenperatura"

#~ msgctxt "@info"
#~ msgid "Percentage of memory taken by applications."
#~ msgstr "Aplikazioek hartutako memoriaren ehunekoa."

#~ msgctxt "@info"
#~ msgid "Percentage of memory taken by the buffer."
#~ msgstr "Tarteko-memoriak («buffer») hartutako memoriaren ehunekoa."

#~ msgctxt "@info"
#~ msgid "Percentage of memory taken by the cache."
#~ msgstr "Cacheak hartutako memoriaren ehunekoa."

#~ msgctxt "@title Free Memory Percentage"
#~ msgid "Free"
#~ msgstr "Hutsa"

#~ msgctxt "@info"
#~ msgid "Percentage of free memory."
#~ msgstr "Memoria hutsaren ehunekoa."

#~ msgctxt "@title Used Memory Percentage"
#~ msgid "Used"
#~ msgstr "Erabilita"

#~ msgctxt "@info"
#~ msgid "Percentage of used memory."
#~ msgstr "Erabilitako memoriaren ehunekoa."

#~ msgctxt "@title"
#~ msgid "Available Memory Percentage"
#~ msgstr "Memoria erabilgarriaren ehunekoa"

#~ msgctxt "@title Available Memory Percentage"
#~ msgid "Available"
#~ msgstr "Erabilgarria"

#~ msgctxt "@info"
#~ msgid "Percentage of available memory."
#~ msgstr "Memoria erabilgarriaren ehunekoa."

#~ msgctxt "@title"
#~ msgid "Allocated Memory Percentage"
#~ msgstr "Alokatutako memoriaren ehunekoa"

#~ msgctxt "@title Allocated Memory Percentage"
#~ msgid "Allocated"
#~ msgstr "Alokatuta"

#~ msgctxt "@info"
#~ msgid "Percentage of allocated memory."
#~ msgstr "Alokatutako memoriaren ehunekoa."

#~ msgctxt "@title Total CPU Usage"
#~ msgid "Usage"
#~ msgstr "Erabilera"

#~ msgctxt "@title Total Memory Usage"
#~ msgid "Total Used"
#~ msgstr "Erabilitako guztizkoa"

#~ msgctxt "@title Cached Memory Usage"
#~ msgid "Cached"
#~ msgstr "Cacheratua"

#~ msgctxt "@title Free Memory Amount"
#~ msgid "Free"
#~ msgstr "Hutsik"

#~ msgctxt "@title Available Memory Amount"
#~ msgid "Available"
#~ msgstr "Erabilgarria"

#~ msgctxt "@title Application Memory Usage"
#~ msgid "Application"
#~ msgstr "Aplikazioa"

#~ msgctxt "@title Buffer Memory Usage"
#~ msgid "Buffer"
#~ msgstr "Tarteko-memoria («buffer»)"

#~ msgctxt "@title Number of Processors"
#~ msgid "Processors"
#~ msgstr "Prozesatzaileak"

#~ msgctxt "@title Number of Cores"
#~ msgid "Cores"
#~ msgstr "Nukleoak"

#~ msgctxt "@title"
#~ msgid "GPU %1 Power Usage"
#~ msgstr "GPU %1, energia erabilera"

#~ msgctxt "@title GPU Power Usage"
#~ msgid "Power"
#~ msgstr "Potentzia"

#~ msgctxt "@title GPU Temperature"
#~ msgid "Temperature"
#~ msgstr "Tenperatura"

#~ msgctxt "@title"
#~ msgid "GPU %1 Shared Memory Usage"
#~ msgstr "GPU %1, memoria partekatuaren erabilera"

#~ msgctxt "@title"
#~ msgid "GPU %1 Encoder Usage"
#~ msgstr "GPU %1, kodetzailearen erabilera"

#~ msgctxt "@title GPU Encoder Usage"
#~ msgid "Encoder"
#~ msgstr "Kodetzailea"

#~ msgctxt "@title"
#~ msgid "GPU %1 Decoder Usage"
#~ msgstr "GPU %1, deskodetzailearen erabilera"

#~ msgctxt "@title GPU Decoder Usage"
#~ msgid "Decoder"
#~ msgstr "Deskodetzailea"

#~ msgctxt "@title"
#~ msgid "GPU %1 Memory Clock"
#~ msgstr "GPU %1, memoriaren erlojua"

#~ msgctxt "@title GPU Memory Clock"
#~ msgid "Memory Clock"
#~ msgstr "Memoriaren erlojua"

#~ msgctxt "@title"
#~ msgid "GPU %1 Processor Clock"
#~ msgstr "GPU %1, prozesatzailearen erlojua"

#~ msgctxt "@title GPU Processor Clock"
#~ msgid "Processor Clock"
#~ msgstr "Prozesatzailearen erlojua"

#~ msgctxt "@title NVidia GPU information"
#~ msgid "NVidia"
#~ msgstr "NVidia"

#~ msgctxt "@title"
#~ msgid "Disk Read Accesses"
#~ msgstr "Diskotik irakurtzeko atzipenak"

#~ msgctxt "@info"
#~ msgid "Read accesses across all disk devices"
#~ msgstr "Disko guztietan irakurtzeko egindako atzipenak"

#~ msgctxt "@title"
#~ msgid "Disk Write Accesses"
#~ msgstr "Diskoan idazteko atzipenak"

#~ msgctxt "@info"
#~ msgid "Write accesses across all disk devices"
#~ msgstr "Disko guztietan idazteko egindako atzipenak"

#~ msgctxt "@title All Network Interfaces"
#~ msgid "All"
#~ msgstr "Guztiak"

#~ msgctxt "@title"
#~ msgid "Received Data Rate"
#~ msgstr "Jasotako datu-emaria"

#~ msgctxt "@info"
#~ msgid "The rate at which data is received on all interfaces."
#~ msgstr "Interfaze guztietan jasotzen diren datuen emaria."

#~ msgctxt "@title"
#~ msgid "Total Received Data"
#~ msgstr "Guztira jasotako datuak"

#~ msgctxt "@info"
#~ msgid "The total amount of data received on all interfaces."
#~ msgstr "Interfaze guztietan jasotako datu-kopuruaren guztizkoa."

#~ msgctxt "@title"
#~ msgid "Sent Data Rate"
#~ msgstr "Bidalitako datu-emaria"

#~ msgctxt "@title Sent Data Rate"
#~ msgid "Up"
#~ msgstr "Gora"

#~ msgctxt "@info"
#~ msgid "The rate at which data is sent on all interfaces."
#~ msgstr "Interfaze guztietan bidaltzen diren datuen emaria."

#~ msgctxt "@title"
#~ msgid "Total Sent Data"
#~ msgstr "Guztira bidalitako datuak"

#~ msgctxt "@info"
#~ msgid "The total amount of data sent on all interfaces."
#~ msgstr "Interfaze guztietan bidalitako datu-kopuruaren guztizkoa."
